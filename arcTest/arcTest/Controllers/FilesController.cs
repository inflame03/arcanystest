﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;

using arcTest.Models;

namespace FileUploadMVC.Controllers
{
    public class FilesController : Controller
    {
        // GET: Upload    
        public ActionResult AsynFileUpload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AsynFileUpload(HttpPostedFileBase file)
        {
            Thread.Sleep(5000);

            if (file != null && file.ContentLength > 0)
            {
                if (file.ContentLength <= 2000000)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                    file.SaveAs(path);

                    ArcFile dbFile = new ArcFile(file.FileName);
                    dbFile.ConvertToByte(file);

                    dbFile.SaveToDB();

                }
                else
                {
                    Session["Error"] = " Maximum file size is 2MB. ";

                }

            }

            return new JsonResult
            {
                Data = "Upload completed!"
            };


        }
    }
}