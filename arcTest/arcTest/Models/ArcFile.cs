﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;


namespace arcTest.Models
{
    public class ArcFile
    {
        private Guid ID { get; set; }
        public string Name { get; set; }
        public byte[] Contents { get; set; }

        public ArcFile( string fname )
        {
            this.ID =Guid.NewGuid();
            this.Name = fname;
        }

        public void ConvertToByte(HttpPostedFileBase file)
        {
            this.Contents = null;
            BinaryReader rdr = new BinaryReader(file.InputStream);
            this.Contents = rdr.ReadBytes((int)file.ContentLength);
        }

        public void SaveToDB()
        {
            SqlConnection cn = new SqlConnection("data source=(LocalDb)\\v12.0;initial catalog=arcTest;integrated security=True;MultipleActiveResultSets=True;");

            SqlCommand cmd = new SqlCommand("INSERT INTO dbo.ArcFile ( ID, Name, Contents) VALUES ( @ID, @NAME, @Content)", cn);

            cmd.Parameters.Add("@ID", SqlDbType.NVarChar, 255).Value = this.ID.ToString();
            cmd.Parameters.Add("@NAME", SqlDbType.NVarChar, 255).Value = this.Name;
            cmd.Parameters.Add("@Content", SqlDbType.VarBinary).Value = this.Contents;

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

    }
}